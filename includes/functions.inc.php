<?php
/**
 * Provides function to basic operations that will be performed frequently
*/
 
    #if i want to make a variable jiska scope pura file ho so i cant just make $variable in the function kyuki phir uska scope bas us function se related hoga instead we can use a keyword 'static' 
    #static variable bhale mera function jhoot jaaye lekin vo apna value preserve karke rakhta hai vo apna scope chordta nahi hai
    # $connection ko echo maarke print nahi kar sakte kyuki vo class mysqli ka object hai so it will give error 
/**
 * @return connection or false
 */
function db_connect()
{
    static $connection;      #ye cheej agar meine 10 baar bhi call kiya to bhi same rahega
    if(!isset($connection))
    {
        $config = parse_ini_file('config.ini');
        $connection = mysqli_connect($config['host'], $config['user'] ,$config['password'], $config['database_name']);
    }
    return $connection;
}
function db_query($query)
{
    $connection = db_connect();
    if($connection)
    {
        $result = mysqli_query($connection, $query);
        return $result;
    }
    return false;
}
function db_error()
{
    $connection = db_connect();
    return mysqli_error($connection);
}

function db_select($query)
{
    $rows = array();
    $result = db_query($query);
    if($result === false)
    {
        return false;
    }

    while($row = mysqli_fetch_assoc($result))
    {
        $rows[] = $row;
    }
    return $rows;
}

function sanitizeData($data){
    $connection = db_connect();
    return mysqli_real_escape_string($connection, $data);
}

function dd($var) {
    die(var_dump($var));
}
